package com.sunhokim.widzard.sunhokimproject.Fragment;

import android.app.DownloadManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.DownloadListener;
import android.webkit.WebView;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.rey.material.widget.EditText;
import com.sunhokim.widzard.sunhokimproject.FeedList.EndlessListViewOnScrollListener;
import com.sunhokim.widzard.sunhokimproject.FeedList.FeedItem;
import com.sunhokim.widzard.sunhokimproject.FeedList.RecyclerViewCommentAdapter;
import com.sunhokim.widzard.sunhokimproject.MainActivity;
import com.sunhokim.widzard.sunhokimproject.R;
import com.sunhokim.widzard.sunhokimproject.app.DataNetworkUtil;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import me.drakeet.materialdialog.MaterialDialog;

/**
 * Created by SunhoKim on 2015-04-03.
 */
public class ListExtendFragment extends Fragment {

    private WebView mWebView;
    private static int position;
    private static List<FeedItem> feedItems;
    private static List<FeedItem> commentItems, newCommentItems, visibleItems;
    private static final String TAG = MainActivity.class.getSimpleName();
    SwipyRefreshLayout mSwipyRefreshLayout;
    private ListView mListView;
    private RecyclerViewCommentAdapter listAdapter;

    private String URL_FEED = "http://WIDzard.pwneo.com/WIDzardCommentList.php";
    private String URL_FEED2 = "http://WIDzard.pwneo.com/WIDzardListProvider.php";
    private String URL_FEED3 = "http://WIDzard.pwneo.com/WIDzardInsertComment.php";
    private String jsonResult, jsonResult2;
    private FeedItem item;
    private EditText mCommentEditText;
    private String commentContent;
    private String beforeCommentContent,afterCommentContent,realCommentContent;
    private boolean isEntered = false;
    private boolean isFirst = true;
    boolean stopLoadMore = false;
    boolean isLoaded = false;
    int loopLimit = 0;
    MaterialDialog mMaterialDialog;


    String[] userData;
    TextView mSignature, mTitle;

    public ListExtendFragment(){}


    public static ListExtendFragment newInstance(List<FeedItem> f,int p)
    {
        ListExtendFragment fragment = new ListExtendFragment();
        position = p;
        feedItems = f;
        return fragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_third_extend, container,
                false);

        View listHeaderView = inflater.inflate(R.layout.listview_header,null);


        item = feedItems.get(position);
        mSwipyRefreshLayout = (SwipyRefreshLayout) rootView.findViewById(R.id.swipyrefreshlayout);
        mSwipyRefreshLayout.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {
                Log.d("MainActivity", "Refresh triggered at "
                        + (direction == SwipyRefreshLayoutDirection.TOP ? "top" : "bottom"));

                mSwipyRefreshLayout.setRefreshing(true);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //Hide the refresh after 2sec
                        MainActivity.mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                commentItems.clear();
                                newCommentItems.clear();
                                visibleItems.clear();
                                isFirst=true;
                                MainActivity.dataNetworkStatus = DataNetworkUtil.getConnectivityStatus(MainActivity.mContext);
                                try{
                                    if(MainActivity.dataNetworkStatus!=0)
                                        accessWebService();
                                    else {
                                        mMaterialDialog = new MaterialDialog(MainActivity.mContext)
                                                .setTitle("인터넷 연결 상태 확인")
                                                .setMessage("인터넷 연결 상태를 확인해주세요.\n")
                                                .setPositiveButton("확인", new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        mMaterialDialog.dismiss();
                                                    }
                                                });
                                        mMaterialDialog.show();
                                    }
                                }catch (Exception e) {

                                    e.printStackTrace();
                                }
                                mSwipyRefreshLayout.setRefreshing(false);
                            }
                        });
                    }
                }, 2000);
            }
        });
        mListView = (ListView) rootView.findViewById(R.id.cardList);
        mListView.addHeaderView(listHeaderView);
        if(MainActivity.isLoginUser)
        {
            View listFooterView = inflater.inflate(R.layout.listview_footer,null);
            mListView.addFooterView(listFooterView);
            Button mButton = (Button) rootView.findViewById(R.id.button);
            mCommentEditText = (EditText) rootView.findViewById(R.id.textfield_et_label);
            mCommentEditText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    if(mCommentEditText.getText().toString().isEmpty()) {
                        realCommentContent=null;
                        afterCommentContent=null;
                        isEntered=false;
                    }
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            mCommentEditText.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if(event.getAction() == KeyEvent.ACTION_DOWN){
                        if(keyCode == KeyEvent.KEYCODE_ENTER){
                            String tempCommentContent = mCommentEditText.getText().toString();
                            if(!isEntered)
                            {
                                realCommentContent = tempCommentContent+"<br />";
                                afterCommentContent=tempCommentContent;
                                isEntered=true;
                            }
                            else
                            {
                                //Log.d(TAG,"before : "+"temp:"+tempCommentContent+"after:"+afterCommentContent+"real:"+realCommentContent);
                                realCommentContent += tempCommentContent.replace(afterCommentContent,"") +"<br />";
                                afterCommentContent=tempCommentContent;
                                //Log.d(TAG,"after : "+"temp:"+tempCommentContent+"after:"+afterCommentContent+"real:"+realCommentContent);
                            }

                        }
                    }
                    return false;
                }


            });
            mButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (!mCommentEditText.getText().equals("") && !isEntered) {
                        commentContent = mCommentEditText.getText().toString();
                        MainActivity.dataNetworkStatus = DataNetworkUtil.getConnectivityStatus(MainActivity.mContext);
                        try{
                            if(MainActivity.dataNetworkStatus!=0){
                                isFirst=false;
                                CommentWriteTask task = new CommentWriteTask();
                                task.execute(new String[]{URL_FEED3});

                            }
                            else {
                                mMaterialDialog = new MaterialDialog(MainActivity.mContext)
                                        .setTitle("인터넷 연결 상태 확인")
                                        .setMessage("인터넷 연결 상태를 확인해주세요.\n")
                                        .setPositiveButton("확인", new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                mMaterialDialog.dismiss();
                                            }
                                        });
                                mMaterialDialog.show();
                            }
                        }catch (Exception e) {

                            e.printStackTrace();
                        }


                    } else if (isEntered) {
                        String tempCommentContent = mCommentEditText.getText().toString();
                        realCommentContent += tempCommentContent.replace(afterCommentContent,"") +"<br />";
                        commentContent = realCommentContent;
                        MainActivity.dataNetworkStatus = DataNetworkUtil.getConnectivityStatus(MainActivity.mContext);
                        try{
                            if(MainActivity.dataNetworkStatus!=0){
                                CommentWriteTask task = new CommentWriteTask();
                                task.execute(new String[]{URL_FEED3});
                            }
                            else {
                                mMaterialDialog = new MaterialDialog(MainActivity.mContext)
                                        .setTitle("인터넷 연결 상태 확인")
                                        .setMessage("인터넷 연결 상태를 확인해주세요.\n")
                                        .setPositiveButton("확인", new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                mMaterialDialog.dismiss();
                                            }
                                        });
                                mMaterialDialog.show();
                            }
                        }catch (Exception e) {

                            e.printStackTrace();
                        }
                    } else {
                        Toast.makeText(MainActivity.mContext, "Error!", Toast.LENGTH_SHORT).show();
                    }
                    mCommentEditText.setText("");
                    isEntered = false;
                }
            });
        }else{
            View listFooterView = inflater.inflate(R.layout.listview_footer_notlogin,null);
            mListView.addFooterView(listFooterView);
        }

        mListView.setOnScrollListener(new EndlessListViewOnScrollListener() {
            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                super.onScroll(view, firstVisibleItem, visibleItemCount, totalItemCount);
                //lastListitem = (totalItemCount - 1 > 0) && (firstVisibleItem + visibleItemCount >= totalItemCount - 1);
            }

            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                //Log.d("TAG", page + "<=page, totalItemsCount=>" + totalItemsCount);
                //Log.e("TAG", visibleItems.size() + "<=visibleItems.size(), newCommentItem.size()=>" + newCommentItems.size());

                if(isLoaded && newCommentItems.size()>0) {

                    Log.d("TAG", page + "<=page, totalItemsCount=>" + totalItemsCount + " visibleItem.Size:" + visibleItems.size());
                    if (newCommentItems.size() < 10) loopLimit = newCommentItems.size();
                    else loopLimit += 10;
                    if(loopLimit>=newCommentItems.size()) loopLimit=newCommentItems.size();


                    Log.d(TAG, "loopLimit:"+loopLimit);
                    if(!stopLoadMore)
                    new PictureReadTask().execute(
                            new String[]{
                                    visibleItems.size() + "",
                                    loopLimit + "",
                            });
                    if(visibleItems.size() > newCommentItems.size())
                    {
                        stopLoadMore = true;
                        loopLimit = 0;
                    }
                    else
                        stopLoadMore = false;
                }

            }
        });

        mWebView = (WebView) rootView.findViewById(R.id.webView);
        mWebView.getSettings().setJavaScriptEnabled(true); // 자바 스크립트 enable
        String userAgent = mWebView.getSettings().getUserAgentString();
        Log.d(TAG, userAgent);
        mSignature = (TextView) rootView.findViewById(R.id.extend_signature);
        mTitle = (TextView) rootView.findViewById(R.id.extend_title);

        mWebView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                final WebView webView = (WebView) v;
                WebView.HitTestResult hr = webView.getHitTestResult();
                final String url = hr.getExtra();
                mMaterialDialog = new MaterialDialog(MainActivity.mContext)
                        .setTitle("이미지 다운로드")
                        .setMessage("확인 버튼을 누르시면 해당 이미지의 다운로드가 시작됩니다.\r\n다운로드된 이미지는 downloads 폴더를 확인하세요.")
                        .setPositiveButton("확인", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //Log.d("TAG", "Long clicked url = " + url);
                                try {
                                    MainActivity.dataNetworkStatus = DataNetworkUtil.getConnectivityStatus(MainActivity.mContext);
                                    if (MainActivity.dataNetworkStatus != 0) {
                                        DownloadManager mDownloadManager = (DownloadManager) MainActivity.mContext.getSystemService(Context.DOWNLOAD_SERVICE);
                                        Uri uri = Uri.parse(url);
                                        DownloadManager.Request mRequest = new DownloadManager.Request(uri);
                                        // mRequest.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI); //NETWORK_MOBILE
                                        // mRequest.setAllowedOverRoaming(false);
                                        mRequest.setTitle("123");
                                        mRequest.setDescription("test");
                                        mRequest.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "123");
                                        File pathExternalPublicDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
                                        pathExternalPublicDir.mkdirs();
                                        // mRequest.setMimeType(HTTP.OCTET_STREAM_TYPE);
                                        mRequest.setShowRunningNotification(true);
                                        mRequest.setVisibleInDownloadsUi(true);
                                        long downloadId = mDownloadManager.enqueue(mRequest);
                                        mMaterialDialog.dismiss();
                                        Toast.makeText(MainActivity.mContext, "다운로드가 시작되었습니다.", Toast.LENGTH_SHORT).show();
                                    } else {
                                        mMaterialDialog = new MaterialDialog(MainActivity.mContext)
                                                .setTitle("인터넷 연결 상태 확인")
                                                .setMessage("인터넷 연결 상태를 확인해주세요.\n")
                                                .setPositiveButton("확인", new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        mMaterialDialog.dismiss();
                                                    }
                                                });
                                        mMaterialDialog.show();
                                    }
                                } catch (Exception e) {

                                    e.printStackTrace();
                                }
                            }
                        })
                        .setNegativeButton("취소", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mMaterialDialog.dismiss();
                                Toast.makeText(MainActivity.mContext, "다운로드가 취소되었습니다.", Toast.LENGTH_SHORT).show();


                            }
                        });

                mMaterialDialog.show();
                return false;

            }
        });
        mWebView.setDownloadListener(new DownloadListener() {

            @Override
            public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimetype, long contentLength) {

                String[] fnm = url.split("/");
                String fname = fnm[fnm.length - 1]; // 파일이름
                String fhost = fnm[2]; // 도메인

                DownloadManager mDownloadManager = (DownloadManager) MainActivity.mContext.getSystemService(Context.DOWNLOAD_SERVICE);
                Uri uri = Uri.parse(url);
                DownloadManager.Request mRequest = new DownloadManager.Request(uri);
                // mRequest.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI); //NETWORK_MOBILE
                // mRequest.setAllowedOverRoaming(false);
                mRequest.setTitle(fname);
                mRequest.setDescription(fhost);
                mRequest.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fname);
                File pathExternalPublicDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
                pathExternalPublicDir.mkdirs();
                // mRequest.setMimeType(HTTP.OCTET_STREAM_TYPE);
                mRequest.setShowRunningNotification(true);
                mRequest.setVisibleInDownloadsUi(true);
                long downloadId = mDownloadManager.enqueue(mRequest);

                Toast.makeText(MainActivity.mContext, "다운로드가 시작되었습니다.", Toast.LENGTH_SHORT).show();

                // 또는 확장자를 판단하여 연계할 앱으로 보내줄 수 있다.
                //                Intent intent = new Intent(Intent.ACTION_VIEW);
                //                intent.setType(mimetype);
                //                intent.setData(Uri.parse(url));
                //                mContext.startActivity(intent);
            }
        });
        newCommentItems = new ArrayList<FeedItem>();
        commentItems = new ArrayList<FeedItem>();
        visibleItems = new ArrayList<FeedItem>();
        listAdapter= new RecyclerViewCommentAdapter(MainActivity.mActivity, visibleItems);
        mListView.setAdapter(listAdapter);
        MainActivity.dataNetworkStatus = DataNetworkUtil.getConnectivityStatus(MainActivity.mContext);
        try{
            if(MainActivity.dataNetworkStatus!=0) {
                isFirst=true;
                accessWebService();

            }
            else {
                mMaterialDialog = new MaterialDialog(MainActivity.mContext)
                        .setTitle("인터넷 연결 상태 확인")
                        .setMessage("인터넷 연결 상태를 확인해주세요.\n")
                        .setPositiveButton("확인", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mMaterialDialog.dismiss();
                            }
                        });
                mMaterialDialog.show();
            }
        }catch (Exception e) {

            e.printStackTrace();
        }
        return rootView;
    }


    public void accessWebService() {

        JsonReadTask task = new JsonReadTask();
        // passes values for the urls string array
        task.execute(new String[]{URL_FEED, URL_FEED2});


    }


    // Async Task to access the web
    private class CommentWriteTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            HttpParams httpParams = new BasicHttpParams();
            httpParams.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
            HttpClient httpclient = new DefaultHttpClient(httpParams);
            HttpPost httppost = new HttpPost(params[0]);
            try {
                //전달할 인자들
                Vector<NameValuePair> nameValue = new Vector<NameValuePair>();
                nameValue.add(new BasicNameValuePair("parent_srl", "0")); //item.getParent_srl()
                nameValue.add(new BasicNameValuePair("module_srl", item.getModule_srl()));
                nameValue.add(new BasicNameValuePair("document_srl", item.getDocument_srl()));
                //nameValue.add(new BasicNameValuePair("member_srl", item.get()));
                nameValue.add(new BasicNameValuePair("content", commentContent));
                nameValue.add(new BasicNameValuePair("user_id", MainActivity.userId));
                nameValue.add(new BasicNameValuePair("password", MainActivity.userPw));
                //Log.d(TAG,nameValue+"=> nameValues");
                //웹 접속 - utf-8 방식으로
                HttpEntity enty = new UrlEncodedFormEntity(nameValue, HTTP.UTF_8);
                httppost.setEntity(enty);
                HttpResponse response = httpclient.execute(httppost);
                jsonResult = inputStreamToString(
                        response.getEntity().getContent()).toString();


            }

            catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
        private StringBuilder inputStreamToString(InputStream is) {
            String rLine = "";
            StringBuilder answer = new StringBuilder();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));

            try {
                while ((rLine = rd.readLine()) != null) {
                    answer.append(rLine);
                }
            }

            catch (IOException e) {
                e.printStackTrace();

            }
            return answer;
        }
        @Override
        protected void onPostExecute(String result) {
            //CheckDataJson();
            Toast.makeText(MainActivity.mContext,"Success!",Toast.LENGTH_SHORT).show();
            mSwipyRefreshLayout.setRefreshing(true);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    //Hide the refresh after 2sec
                    MainActivity.mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            accessWebService();
                            mWebView.loadDataWithBaseURL(null, item.getTxtHtml(), "text/html", "UTF-8", null);
                            mSwipyRefreshLayout.setRefreshing(false);
                        }
                    });
                }
            }, 2000);
        }
    }// end async task
    // Async Task to access the web
    private class JsonReadTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(params[0]);
            try {
                //전달할 인자들
                Vector<NameValuePair> nameValue = new Vector<NameValuePair>();
                nameValue.add(new BasicNameValuePair("document_srl", item.getDocument_srl()));

                nameValue.add(new BasicNameValuePair("user_id", item.getUser_id()));
                nameValue.add(new BasicNameValuePair("member_srl", item.getMember_srl() + ""));
                //웹 접속 - utf-8 방식으로
                HttpEntity enty = new UrlEncodedFormEntity(nameValue, HTTP.UTF_8);
                httppost.setEntity(enty);
                HttpResponse response = httpclient.execute(httppost);
                //jsonResult = inputStreamToString(response.getEntity().getContent()).toString();
                userData = inputStreamToString(
                        response.getEntity().getContent()).toString().split("_&_");
                userData[0] = userData[0].replaceAll("\uFEFF", "");
                item.setSignature(userData[0]);
                return userData[1];
            }

            catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }
        private StringBuilder inputStreamToString(InputStream is) {
            String rLine = "";
            StringBuilder answer = new StringBuilder();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));

            try {
                while ((rLine = rd.readLine()) != null) {
                    answer.append(rLine);
                }
            }

            catch (IOException e) {
                e.printStackTrace();

            }
            return answer;
        }
        @Override
        protected void onPostExecute(String result) {
            CheckDataJson(result);
            Log.e(TAG, "CheckDatajson() done");
            mSignature.setText(item.getSignature());
            Log.e(TAG, item.getStatus());
            mTitle.setText(item.getStatus());
            mWebView.loadDataWithBaseURL(null, item.getTxtHtml(), "text/html", "UTF-8", null);
        }
    }// end async task
    String new_member_srl = null;
    int member_srl = 0;
    private void CheckDataJson(String jsonResult) {
        int new_position_id=0;
        try {

            jsonResult = jsonResult.replaceAll("<br />","\r\n");
            jsonResult = jsonResult.replaceAll("&nbsp;"," ");
            jsonResult = jsonResult.replaceAll("&#39;","'");
            JSONObject jsonResponse = new JSONObject(jsonResult);
            JSONArray jsonMainNode = jsonResponse.optJSONArray("document");
            int position_id = 0;


            for (int i = 0; i < jsonMainNode.length() ; i++) {
                JSONObject jsonChildNode = jsonMainNode.getJSONObject(i);

                new_member_srl = null;
                member_srl = Integer.parseInt(jsonChildNode.optString("member_srl"));
                if(member_srl < 10)
                    new_member_srl = "00"+member_srl;
                else if (member_srl < 100)
                    new_member_srl = "0"+member_srl;
                else
                    new_member_srl = ""+member_srl;
                String last_update=jsonChildNode.optString("last_update");
                String year = last_update.substring(0,4);
                String month = last_update.substring(4,6);
                String day = last_update.substring(6,8);
                String hour = last_update.substring(8,10);
                String minute = last_update.substring(10,12);
                String second = last_update.substring(12,14);
                String convertTime = year+"-"+month+"-"+day+" "+hour+":"+minute+":"+second;
                String document_srl = jsonChildNode.optString("document_srl");
                if(isFirst && document_srl.equalsIgnoreCase(item.getDocument_srl())) {

                        FeedItem commentItem = new FeedItem();

                        commentItem.setId(position_id++);
                        commentItem.setNew_member_srl(new_member_srl);
                        commentItem.setMember_srl(member_srl);

                        if(commentItem.getMember_srl()==item.getMember_srl()) {
                            Log.e(TAG, ""+commentItem.getMember_srl()+"//"+item.getMember_srl()+"//id:"+commentItem.getId());
                            commentItem.setIsMine(true);
                        }
                        else commentItem.setIsMine(false);

                        commentItem.setDocument_srl(jsonChildNode.optString("document_srl"));
                        commentItem.setName(jsonChildNode.optString("nick_name"));
                        commentItem.setStatus(jsonChildNode.optString("content").replace("<br />", "\r\n"));
                        commentItem.setComment_srl(jsonChildNode.optString("comment_srl"));
                        commentItem.setParent_srl(jsonChildNode.optString("parent_srl"));
                        commentItem.setList_order(jsonChildNode.optString("list_order"));
                        commentItem.setIsSorted(false);

                        try {
                            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                            Date parsedDate = dateFormat.parse(convertTime);
                            Timestamp timeStamp = new java.sql.Timestamp(parsedDate.getTime());
                            commentItem.setTimeStamp(parsedDate.getTime() + "");
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    commentItems.add(commentItem);
                }else if(!isFirst && document_srl.equalsIgnoreCase(item.getDocument_srl())){
                    new_position_id = newCommentItems.get(newCommentItems.size()-1).getId()+1;
                    if(++position_id == new_position_id){
                        FeedItem commentItem = new FeedItem();

                        commentItem.setId(new_position_id);
                        commentItem.setNew_member_srl(new_member_srl);
                        commentItem.setMember_srl(member_srl);

                        if(commentItem.getMember_srl()==item.getMember_srl()) {
                            Log.e(TAG, ""+commentItem.getMember_srl()+"//"+item.getMember_srl()+"//id:"+commentItem.getId());
                            commentItem.setIsMine(true);
                        }
                        else commentItem.setIsMine(false);

                        commentItem.setDocument_srl(jsonChildNode.optString("document_srl"));
                        commentItem.setName(jsonChildNode.optString("nick_name"));
                        commentItem.setStatus(jsonChildNode.optString("content").replace("<br />", "\r\n"));
                        commentItem.setComment_srl(jsonChildNode.optString("comment_srl"));
                        commentItem.setParent_srl(jsonChildNode.optString("parent_srl"));
                        commentItem.setList_order(jsonChildNode.optString("list_order"));
                        commentItem.setIsSorted(false);

                        try {
                            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                            Date parsedDate = dateFormat.parse(convertTime);
                            Timestamp timeStamp = new java.sql.Timestamp(parsedDate.getTime());
                            commentItem.setTimeStamp(parsedDate.getTime() + "");
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        commentItems.add(commentItem);
                    }

                }else continue;
            }
            // 대댓글 Sorting
                for (int i = 0; i < commentItems.size(); i++) {
                    for (int j = 0; j < commentItems.size(); j++) {
                        if (Integer.parseInt(commentItems.get(i).getList_order()) > Integer.parseInt(commentItems.get(j).getList_order())) {
                            FeedItem temp;
                            temp = commentItems.get(i);
                            commentItems.set(i, commentItems.get(j));
                            commentItems.set(j, temp);
                        }
                    }

                }
                for (int i = 0; i < commentItems.size(); i++) {
                    FeedItem firstItem = commentItems.get(i);
                    if (firstItem.getIsSorted()) continue;
                    else if (!firstItem.getIsSorted() && firstItem.getParent_srl().equalsIgnoreCase("0")) {
                        newCommentItems.add(firstItem);
                        commentItems.get(i).setIsSorted(true);
                    }
                    for (int j = i + 1; j < commentItems.size(); j++) {
                        FeedItem secondItem = commentItems.get(j);
                        if (firstItem.getComment_srl().equalsIgnoreCase(secondItem.getParent_srl())) {
                            secondItem.setStatus("[" + firstItem.getName() + "] " + secondItem.getStatus());
                            newCommentItems.add(secondItem);
                            commentItems.get(j).setIsSorted(true);
                            firstItem = secondItem;
                        }
                    }


            }
            int startNumber = 0 ;
            if(!isFirst) {
                startNumber = newCommentItems.size()-1;
                loopLimit = newCommentItems.size();
                Log.d(TAG,startNumber+"/"+loopLimit+"/new:"+new_position_id);
            }else {
                if (newCommentItems.size() < 10) loopLimit = newCommentItems.size();
                else loopLimit = 10;
                isFirst=false;
            }
            new PictureReadTask().execute(
                    new String[]{
                            startNumber + "",
                            loopLimit + ""
                    });
        } catch (JSONException e) {
            Log.d(TAG, "Error" + e.toString());
        }
    }

    private class PictureReadTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            isLoaded=false;
            String profilePath = null;
            for(int i=Integer.parseInt(params[0]);i<Integer.parseInt(params[1]);i++) {
                URL url = null;
                try {
                    url = new URL("http://WIDzard.pwneo.com/files/member_extra_info/profile_image/"
                            + newCommentItems.get(i).getNew_member_srl() + "/" + newCommentItems.get(i).getMember_srl() + ".png");

                    URLConnection con = url.openConnection();
                    HttpURLConnection exitCode = (HttpURLConnection) con;

                    if (exitCode.getResponseCode() == 404) {
                        url = new URL("http://WIDzard.pwneo.com/files/member_extra_info/profile_image/"
                                + newCommentItems.get(i).getNew_member_srl() + "/" + newCommentItems.get(i).getMember_srl() + ".gif");
                        con = url.openConnection();
                        exitCode = (HttpURLConnection) con;
                    }

                    if (exitCode.getResponseCode() == 200 && con.getContentType().equals("image/png") && con.getContentLength() > 0) {
                        profilePath = "http://WIDzard.pwneo.com/files/member_extra_info/profile_image/"
                                + newCommentItems.get(i).getNew_member_srl() + "/" + newCommentItems.get(i).getMember_srl() + ".png";
                    } else if (exitCode.getResponseCode() == 200 && con.getContentType().equals("image/gif") && con.getContentLength() > 0) {
                        profilePath = "http://WIDzard.pwneo.com/files/member_extra_info/profile_image/"
                                + newCommentItems.get(i).getNew_member_srl() + "/" + newCommentItems.get(i).getMember_srl() + ".gif";

                    } else {
                        profilePath = "";
                    }
                    newCommentItems.get(i).setProfilePic(profilePath);
                    visibleItems.add(newCommentItems.get(i));
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return profilePath;
        }
        @Override
        protected void onPostExecute(String result) {
            Log.e(TAG,"PictureReadTask Done");
            listAdapter.notifyDataSetChanged();
            isLoaded=true;
        }
    }


}
