package com.sunhokim.widzard.sunhokimproject.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.github.ksoichiro.android.observablescrollview.ObservableListView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;
import com.sunhokim.widzard.sunhokimproject.R;

import java.util.ArrayList;

/**
 * Created by SunhoKim on 2015-04-27.
 */
public class SecondFragment extends Fragment implements ObservableScrollViewCallbacks {


    public SecondFragment(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_second, container,
                false);
        ObservableListView listView = (ObservableListView) rootView.findViewById(R.id.list);
        listView.setScrollViewCallbacks(this);
        setDummyData(listView);
        return rootView;
        }
    protected void setDummyData(ListView listView) {
        listView.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, getDummyData()));
    }
    private static final int NUM_OF_ITEMS = 100;
    public static ArrayList<String> getDummyData() {
        return getDummyData(NUM_OF_ITEMS);
    }
    public static ArrayList<String> getDummyData(int num) {
        ArrayList<String> items = new ArrayList<String>();
        for (int i = 1; i <= num; i++) {
            items.add("Item " + i);
        }
        return items;
    }
    @Override
    public void onScrollChanged(int i, boolean b, boolean b2) {

    }

    @Override
    public void onDownMotionEvent() {

    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {
        ActionBarActivity activity = (ActionBarActivity) getActivity();
        if (activity == null) {
            return;
        }
        ActionBar ab = activity.getSupportActionBar();
        if (scrollState == ScrollState.UP) {
            if (ab.isShowing()) {
                ab.hide();
            }
        } else if (scrollState == ScrollState.DOWN) {
            if (!ab.isShowing()) {
                ab.show();
            }
        }
    }
}
