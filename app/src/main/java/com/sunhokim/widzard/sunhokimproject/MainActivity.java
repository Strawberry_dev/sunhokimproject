package com.sunhokim.widzard.sunhokimproject;


import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.readystatesoftware.systembartint.SystemBarTintManager;
import com.sunhokim.widzard.sunhokimproject.Fragment.FlexibleSpaceWithImageScrollViewFragment;
import com.sunhokim.widzard.sunhokimproject.Fragment.LoginFragment;
import com.sunhokim.widzard.sunhokimproject.Fragment.SettingFragment;
import com.sunhokim.widzard.sunhokimproject.Fragment.ListFragment;
import com.sunhokim.widzard.sunhokimproject.NavigationDrawer.NavigationDrawerCallbacks;
import com.sunhokim.widzard.sunhokimproject.NavigationDrawer.NavigationDrawerFragment;
import com.sunhokim.widzard.sunhokimproject.app.DataNetworkUtil;


public class MainActivity extends ActionBarActivity
        implements NavigationDrawerCallbacks {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    public static NavigationDrawerFragment mNavigationDrawerFragment;
    public static boolean isLoginUser = false;
    public static Toolbar mToolbar;
    public static Context mContext;
    public static Activity mActivity;
    public static String userNickName="Guest";
    public static String userId, userPw, userProfilePic;
    public static boolean autoLogin;
    public static boolean isClickedItem = false;

    private LinearLayout toolbarLayout;
    public static int dataNetworkStatus = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext=this;
        mActivity=this;
        toolbarLayout = (LinearLayout) findViewById(R.id.toolbarLayout);
        dataNetworkStatus = DataNetworkUtil.getConnectivityStatus(mContext);
        if(android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
            // create our manager instance after the content view is set
            SystemBarTintManager tintManager = new SystemBarTintManager(this);
            // enable status bar tint
            tintManager.setStatusBarTintEnabled(true);
            // enable navigation bar tint
            tintManager.setNavigationBarTintEnabled(true);
            // set a custom tint color for all system bars
            tintManager.setStatusBarTintColor(Color.parseColor("#DD000000"));
            // set a custom navigation bar resource
            //tintManager.setNavigationBarTintResource(R.drawable.my_tint);
            // set a custom status bar drawable
            //tintManager.setStatusBarTintDrawable(MyDrawable);
            toolbarLayout.setVisibility(View.VISIBLE);
        } else
            toolbarLayout.setVisibility(View.GONE);


        mToolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(mToolbar);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getFragmentManager().findFragmentById(R.id.fragment_drawer);

        // Set up the drawer.
        mNavigationDrawerFragment.setup(R.id.fragment_drawer, (DrawerLayout) findViewById(R.id.drawer), mToolbar);
        // populate the navigation drawer
        mNavigationDrawerFragment.setUserData("손님으로 접속하셨습니다.", "어서오세요.", "http://WIDzard.zz.am/files/member_extra_info/profile_image/007/7.gif");

        SharedPreferences pref = getSharedPreferences("pref", Activity.MODE_PRIVATE);
        userId = pref.getString("id","");
        userPw = pref.getString("password","");
        autoLogin = pref.getBoolean("auto", false);
        Log.d("TAG","pref Result=> "+userId+", "+userPw+", "+autoLogin);



        if(autoLogin && !userId.isEmpty() && !userPw.isEmpty() && dataNetworkStatus!=0)
        {
            Log.d("TAG", "Begin Fragment() On onCreate()");
            final LoginFragment f = new LoginFragment();
            f.newInstance(userId, userPw, autoLogin);
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().add(R.id.container,f).commit();
            fragmentManager.executePendingTransactions();
            if(f.isAdded()) {
                fragmentManager.beginTransaction().remove(f).commit();
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        mNavigationDrawerFragment.setUserData(MainActivity.userNickName, "어서오세요.", userProfilePic);
                    }
                }, 500);
            }
            Log.d("TAG", "End Fragment() On onCreate()");
            //SecondFragment f = new SecondFragment(pref.getString("id",""), pref.getString("password",""),pref.getBoolean("auto",false));
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        ListFragment fragment = null;
        Log.d("TAG", "Begin Fragment() On Navigation Menu : "+position);
    /*
     * Module_srl List
     *
     * Notice : 4142
     * Kernel : 109
     * Recovery : 349
     * FreeBoard : 153
     * Firmware(Test) : 2264
     */
        switch (position) {
            case 0:
                fragment = new ListFragment();
                fragment.newInstance("4142");
                break;
            case 1:
                fragment = new ListFragment();
                fragment.newInstance("109");
                break;
            case 2:
                fragment = new ListFragment();
                fragment.newInstance("349");
                break;
            case 3:
                fragment = new ListFragment();
                fragment.newInstance("153");
                break;
            default:
                fragment = new ListFragment();
                fragment.newInstance("4142");
                break;
        }
        if (fragment != null) {
            Log.d("TAG", "Begin Fragment() On Navigation Menu");
            FragmentManager fragmentManager = getSupportFragmentManager();

            fragmentManager.beginTransaction()

                    .replace(R.id.container, fragment)
                    .commit();


        } else {
            Log.e("MainActivity", "Error in creating fragment");
        }
        Toast.makeText(this, "Menu item selected -> " + position, Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onBackPressed() {

        if (mNavigationDrawerFragment.isDrawerOpen())
            mNavigationDrawerFragment.closeDrawer();
        else
            super.onBackPressed();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Fragment fragment = null;
            fragment = new SettingFragment();

            if (fragment != null) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.container, fragment)
                        .addToBackStack("null")
                        .commit();


            } else {
                Log.e("MainActivity", "Error in creating fragment");
            }
        }

        return super.onOptionsItemSelected(item);
    }


}
