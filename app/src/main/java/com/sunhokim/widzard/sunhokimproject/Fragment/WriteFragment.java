package com.sunhokim.widzard.sunhokimproject.Fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.widget.Toast;

import com.dd.processbutton.iml.ActionProcessButton;
import com.rey.material.widget.EditText;
import com.sunhokim.widzard.sunhokimproject.FeedList.FeedItem;
import com.sunhokim.widzard.sunhokimproject.MainActivity;
import com.sunhokim.widzard.sunhokimproject.ProgressGenerator;
import com.sunhokim.widzard.sunhokimproject.R;
import com.sunhokim.widzard.sunhokimproject.app.DataNetworkUtil;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Vector;

import me.drakeet.materialdialog.MaterialDialog;

/**
 * Created by SunhoKim on 2015-05-04.
 */
public class WriteFragment extends Fragment implements ProgressGenerator.OnCompleteListener{

    private static List<FeedItem> feedItems;
    ActionProcessButton btnSignIn;
    ProgressGenerator progressGenerator;
    EditText title,content;
    String mTitle, mContent;

    private static MaterialDialog mMaterialDialog;
    public WriteFragment(){}

    public static WriteFragment newInstance(List<FeedItem> f)
    {
        WriteFragment fragment = new WriteFragment();
        feedItems = f;
        //fragment.writeTask("http://WIDzard.zz.am/WIDzardInsertDocument.php",f);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_write_document, container,
                false);
        title = (EditText) rootView.findViewById(R.id.title);
        content = (EditText) rootView.findViewById(R.id.content);
        progressGenerator = new ProgressGenerator(this);
        btnSignIn = (ActionProcessButton) rootView.findViewById(R.id.btnSignIn);
        btnSignIn.setMode(ActionProcessButton.Mode.ENDLESS);
        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!MainActivity.userId.isEmpty() && !title.getText().toString().isEmpty() && !content.getText().toString().isEmpty()) {
                    mTitle = title.getText().toString();
                    mContent = content.getText().toString();
                    progressGenerator.start(btnSignIn);
                    MainActivity.dataNetworkStatus = DataNetworkUtil.getConnectivityStatus(MainActivity.mContext);
                    try{
                        if(MainActivity.dataNetworkStatus!=0)
                            writeTask("http://WIDzard.zz.am/WIDzardInsertDocument.php", feedItems);
                        else {
                            mMaterialDialog = new MaterialDialog(MainActivity.mContext)
                                    .setTitle("인터넷 연결 상태 확인")
                                    .setMessage("인터넷 연결 상태를 확인해주세요.\n")
                                    .setPositiveButton("확인", new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            mMaterialDialog.dismiss();
                                        }
                                    });
                            mMaterialDialog.show();
                        }
                    }catch (Exception e) {

                        e.printStackTrace();
                    }
                }else
                    Toast.makeText(MainActivity.mContext,"로그인을 먼저 해주세요.",Toast.LENGTH_SHORT).show();
            }
        });
        return rootView;
    }
    private StringBuilder inputStreamToString(InputStream is) {
        String rLine = "";
        StringBuilder answer = new StringBuilder();
        BufferedReader rd = new BufferedReader(new InputStreamReader(is));

        try {
            while ((rLine = rd.readLine()) != null) {
                answer.append(rLine);
            }
        }

        catch (IOException e) {
            e.printStackTrace();

        }
        return answer;
    }
    public void writeTask(String URL_FEED,List<FeedItem> feedItems) {
        HttpParams httpParams = new BasicHttpParams();
        httpParams.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
        HttpClient httpclient = new DefaultHttpClient(httpParams);
        HttpPost httppost = new HttpPost(URL_FEED);
        try {
            //전달할 인자들
            Vector<NameValuePair> nameValue = new Vector<NameValuePair>();
            nameValue.add(new BasicNameValuePair("user_id", MainActivity.userId));
            nameValue.add(new BasicNameValuePair("password", MainActivity.userPw));
            nameValue.add(new BasicNameValuePair("module_srl", "143"));
            nameValue.add(new BasicNameValuePair("title", mTitle));
            nameValue.add(new BasicNameValuePair("content", mContent));
            //웹 접속 - utf-8 방식으로
            HttpEntity enty = new UrlEncodedFormEntity(nameValue, HTTP.UTF_8);
            httppost.setEntity(enty);
            HttpResponse response = httpclient.execute(httppost);
            String jsonResult = inputStreamToString(
                    response.getEntity().getContent()).toString();


        }

        catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }



    }

    @Override
    public void onComplete() {
        Toast.makeText(MainActivity.mContext, "글쓰기가 완료되었습니다.", Toast.LENGTH_SHORT).show();
        FragmentManager fm = getFragmentManager();
        fm.popBackStack();
    }
}
