package com.sunhokim.widzard.sunhokimproject.Fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.melnykov.fab.FloatingActionButton;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.sunhokim.widzard.sunhokimproject.FeedList.EndlessRecyclerOnScrollListener;
import com.sunhokim.widzard.sunhokimproject.FeedList.FeedItem;
import com.sunhokim.widzard.sunhokimproject.FeedList.RecyclerViewListAdapter;
import com.sunhokim.widzard.sunhokimproject.FeedList.RecyclerViewItemClickListener;
import com.sunhokim.widzard.sunhokimproject.MainActivity;
import com.sunhokim.widzard.sunhokimproject.R;
import com.sunhokim.widzard.sunhokimproject.app.DataNetworkUtil;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import me.drakeet.materialdialog.MaterialDialog;

/**
 * Created by SunhoKim on 2015-04-02.
 */
public class ListFragment extends Fragment {
    private static final String TAG = MainActivity.class.getSimpleName();
    private RecyclerView mRecyclerView;
    private List<FeedItem> feedItems, noticeItems, newFeedItems, visibleItems;
    private RecyclerViewListAdapter listAdapter;
    private String URL_FEED = "http://WIDzard.pwneo.com/WIDzardListProvider.php";

    private String jsonResult;
    private Toolbar mToolbar;
    private SwipyRefreshLayout mSwipyRefreshLayout;
    MaterialDialog mMaterialDialog;
    public static String MODULE_SRL="4142";

    private boolean isFirst = true;
    boolean stopLoadMore = false;
    boolean isLoaded = false;
    public ListFragment()
    {

    }
    public static ListFragment newInstance(String M) {
        ListFragment fragment = new ListFragment();
        MODULE_SRL = M;
        return fragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_third, container,
                false);
        Log.d("TAG","MODULE_SRL="+MODULE_SRL);
        //mToolbar = (Toolbar)rootView.findViewById(R.id.toolbar_actionbar);
        //((ActionBarActivity) getActivity()).setSupportActionBar(mToolbar);
        mSwipyRefreshLayout = (SwipyRefreshLayout) rootView.findViewById(R.id.swipyrefreshlayout);
        mSwipyRefreshLayout.setOnRefreshListener(new SwipyRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh(SwipyRefreshLayoutDirection direction) {
                Log.d("MainActivity", "Refresh triggered at "
                        + (direction == SwipyRefreshLayoutDirection.TOP ? "top" : "bottom"));

                mSwipyRefreshLayout.setRefreshing(true);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //Hide the refresh after 2sec
                        MainActivity.mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                newFeedItems.clear();
                                feedItems.clear();
                                noticeItems.clear();
                                visibleItems.clear();
                                isFirst=true;
                                MainActivity.dataNetworkStatus = DataNetworkUtil.getConnectivityStatus(MainActivity.mContext);
                                try{
                                    if(MainActivity.dataNetworkStatus!=0)
                                        accessWebService();
                                    else {
                                        mMaterialDialog = new MaterialDialog(MainActivity.mContext)
                                                .setTitle("인터넷 연결 상태 확인")
                                                .setMessage("인터넷 연결 상태를 확인해주세요.\n")
                                                .setPositiveButton("확인", new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        mMaterialDialog.dismiss();
                                                    }
                                                });
                                        mMaterialDialog.show();
                                    }
                                }catch (Exception e) {

                                    e.printStackTrace();
                                }
                                mSwipyRefreshLayout.setRefreshing(false);
                            }
                        });
                    }
                }, 2000);
            }
        });
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.cardList);
        mRecyclerView.addOnItemTouchListener(new RecyclerViewItemClickListener(MainActivity.mActivity, new RecyclerViewItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        MainActivity.dataNetworkStatus = DataNetworkUtil.getConnectivityStatus(MainActivity.mContext);
                        try {
                            if (MainActivity.dataNetworkStatus != 0) {
                                // do whatever
                                MainActivity.isClickedItem = true;
                                getActivity().getSupportFragmentManager()
                                        .beginTransaction()
                                        .replace(R.id.container, ListExtendFragment.newInstance(visibleItems, position))
                                        .addToBackStack("null").commit();
                            } else {
                                mMaterialDialog = new MaterialDialog(MainActivity.mContext)
                                        .setTitle("인터넷 연결 상태 확인")
                                        .setMessage("인터넷 연결 상태를 확인해주세요.\n")
                                        .setPositiveButton("확인", new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                mMaterialDialog.dismiss();
                                            }
                                        });
                                mMaterialDialog.show();
                            }
                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                    }
                })
        );

        mRecyclerView.setHasFixedSize(true);

        LinearLayoutManager llm = new LinearLayoutManager(MainActivity.mActivity);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(llm);

            feedItems = new ArrayList<FeedItem>();
            noticeItems = new ArrayList<FeedItem>();
            newFeedItems = new ArrayList<FeedItem>();
            visibleItems = new ArrayList<FeedItem>();

        listAdapter= new RecyclerViewListAdapter(MainActivity.mActivity, visibleItems);
        mRecyclerView.setAdapter(listAdapter);
        mRecyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener() {
            @Override
            public void onLoadMore(int current_page) {


                if(isLoaded && newFeedItems.size()>0) {

                    if (newFeedItems.size() < 10) loopLimit = newFeedItems.size();
                    else loopLimit += 10;
                    if(loopLimit>=newFeedItems.size()) loopLimit=newFeedItems.size();


                    Log.d(TAG, "loopLimit:"+loopLimit);
                    if(!stopLoadMore)
                        new PictureReadTask().execute(
                                new String[]{
                                        visibleItems.size() + "",
                                        loopLimit + "",
                                });
                    if(visibleItems.size() > newFeedItems.size())
                    {
                        stopLoadMore = true;
                        loopLimit = 0;
                    }
                    else
                        stopLoadMore = false;
                }
            }
        });
        FloatingActionButton fab = (FloatingActionButton) rootView.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.container, WriteFragment.newInstance(newFeedItems))
                        .addToBackStack("write").commit();
            }
        });
        fab.attachToRecyclerView(mRecyclerView);
        fab.show();
        MainActivity.dataNetworkStatus = DataNetworkUtil.getConnectivityStatus(MainActivity.mContext);
        try{
            if(MainActivity.dataNetworkStatus!=0)
                accessWebService();
            else {
                mMaterialDialog = new MaterialDialog(MainActivity.mContext)
                        .setTitle("인터넷 연결 상태 확인")
                        .setMessage("인터넷 연결 상태를 확인해주세요.\n")
                        .setPositiveButton("확인", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mMaterialDialog.dismiss();
                            }
                        });
                mMaterialDialog.show();
            }
        }catch (Exception e) {

            e.printStackTrace();
        }


        return rootView;
    }

    public void accessWebService() {
        JsonReadTask task = new JsonReadTask();
        // passes values for the urls string array
        task.execute(new String[] { URL_FEED });

    }
    // Async Task to access the web
    private class JsonReadTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            HttpParams httpParams = new BasicHttpParams();
            httpParams.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
            HttpClient httpclient = new DefaultHttpClient(httpParams);
            HttpPost httppost = new HttpPost(params[0]);
            try {
                HttpResponse response = httpclient.execute(httppost);
                jsonResult = inputStreamToString(
                        response.getEntity().getContent()).toString();
            }

            catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {

                e.printStackTrace();
            }
            return null;
        }
        private StringBuilder inputStreamToString(InputStream is) {
            String rLine = "";
            StringBuilder answer = new StringBuilder();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));

            try {
                while ((rLine = rd.readLine()) != null) {
                    answer.append(rLine);
                }
            }

            catch (IOException e) {
                e.printStackTrace();

            }
            return answer;
        }
        @Override
        protected void onPostExecute(String result) {
            CheckDataJson();
        }
    }// end async task


    String new_member_srl = null;
    int member_srl = 0;
    int loopLimit = 0;
    private void CheckDataJson() {
        int new_position_id=0;
        try {

            JSONObject jsonResponse = new JSONObject(jsonResult.replaceAll("<br />","<br>"));
            //Log.d(TAG,jsonResult.replaceAll("<br />","<br>"));
            JSONArray jsonMainNode = jsonResponse.optJSONArray("document");

            int position_id = 0;
            for (int i = 0; i < jsonMainNode.length(); i++) {
                JSONObject jsonChildNode = jsonMainNode.getJSONObject(i);
                String module_srl = jsonChildNode.optString("module_srl");
                member_srl = Integer.parseInt(jsonChildNode.optString("member_srl"));
                if(member_srl < 10)
                    new_member_srl = "00"+member_srl;
                else if (member_srl < 100)
                    new_member_srl = "0"+member_srl;
                else
                    new_member_srl = ""+member_srl;
                String last_update=jsonChildNode.optString("last_update");
                String year = last_update.substring(0,4);
                String month = last_update.substring(4,6);
                String day = last_update.substring(6,8);
                String hour = last_update.substring(8,10);
                String minute = last_update.substring(10,12);
                String second = last_update.substring(12,14);
                String convertTime = year+"-"+month+"-"+day+" "+hour+":"+minute+":"+second;
                String is_notice = jsonChildNode.optString("is_notice");
                if(module_srl.equalsIgnoreCase(MODULE_SRL)) {
                    final FeedItem item = new FeedItem();
                    item.setId(position_id++);
                    item.setNew_member_srl(new_member_srl);
                    item.setMember_srl(member_srl);
                    item.setDocument_srl(jsonChildNode.optString("document_srl"));
                    item.setModule_srl(jsonChildNode.optString("module_srl"));
                    item.setName(jsonChildNode.optString("nick_name"));
                    item.setStatus(jsonChildNode.optString("title"));
                    item.setReadCount(jsonChildNode.optString("readed_count"));
                    item.setCommentCount(jsonChildNode.optString("comment_count"));
                    item.setTxtHtml((jsonChildNode.optString("content")));
                    item.setList_order(jsonChildNode.optString("list_order"));
                    item.setIs_notice(jsonChildNode.optString("is_notice"));
                    item.setUser_id(jsonChildNode.optString("user_id"));
                    try {
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                        Date parsedDate = dateFormat.parse(convertTime);
                        Timestamp timeStamp = new java.sql.Timestamp(parsedDate.getTime());
                        item.setTimeStamp(parsedDate.getTime()+"");
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    if(is_notice.equalsIgnoreCase("N"))
                        feedItems.add(item);
                    else
                        noticeItems.add(item);
                }else continue;
            }
            for(int i=0; i < noticeItems.size() ; i++) {
                for (int j=0; j < noticeItems.size(); j++)
                {
                    if(Double.parseDouble(noticeItems.get(i).getList_order()) < Double.parseDouble(noticeItems.get(j).getList_order()))
                    {
                        FeedItem temp;
                        temp = noticeItems.get(i);
                        noticeItems.set(i, noticeItems.get(j));
                        noticeItems.set(j, temp);
                    }
                }
            }
            for(int i=0; i < noticeItems.size() ; i++) {
                newFeedItems.add(noticeItems.get(i));
            }
            for(int i=0; i < feedItems.size() ; i++) {
                for (int j=0; j < feedItems.size(); j++)
                {
                   if((Double.parseDouble(feedItems.get(i).getList_order()) < Double.parseDouble(feedItems.get(j).getList_order()))
                           && feedItems.get(i).getIs_notice().equals("N"))
                    {
                        FeedItem temp;
                        temp = feedItems.get(i);
                        feedItems.set(i, feedItems.get(j));
                        feedItems.set(j, temp);
                    }
                }
            }
            for(int i=0; i < feedItems.size() ; i++) {
                newFeedItems.add(feedItems.get(i));
            }
            int startNumber = 0 ;
            if(!isFirst) {
                startNumber = newFeedItems.size()-1;
                loopLimit = newFeedItems.size();
                Log.d(TAG,startNumber+"/"+loopLimit+"/new:"+new_position_id);
            }else {
                if (newFeedItems.size() < 10) loopLimit = newFeedItems.size();
                else loopLimit = 10;
                isFirst=false;
            }
            new PictureReadTask().execute(
                    new String[]{
                            startNumber + "",
                            loopLimit + ""
                    });
        } catch (JSONException e) {
            Log.d(TAG, "Error!!" + e.toString());
        }
    }


    private class PictureReadTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            isLoaded=false;
            String profilePath = null;
            for(int i=Integer.parseInt(params[0]);i<Integer.parseInt(params[1]);i++) {
                URL url = null;
                try {
                    url = new URL("http://WIDzard.pwneo.com/files/member_extra_info/profile_image/"
                            + newFeedItems.get(i).getNew_member_srl() + "/" + newFeedItems.get(i).getMember_srl() + ".png");

                    URLConnection con = url.openConnection();
                    HttpURLConnection exitCode = (HttpURLConnection) con;

                    if (exitCode.getResponseCode() == 404) {
                        url = new URL("http://WIDzard.pwneo.com/files/member_extra_info/profile_image/"
                                + newFeedItems.get(i).getNew_member_srl() + "/" + newFeedItems.get(i).getMember_srl() + ".gif");
                        con = url.openConnection();
                        exitCode = (HttpURLConnection) con;
                    }

                    if (exitCode.getResponseCode() == 200 && con.getContentType().equals("image/png") && con.getContentLength() > 0) {
                        profilePath = "http://WIDzard.pwneo.com/files/member_extra_info/profile_image/"
                                + newFeedItems.get(i).getNew_member_srl() + "/" + newFeedItems.get(i).getMember_srl() + ".png";
                    } else if (exitCode.getResponseCode() == 200 && con.getContentType().equals("image/gif") && con.getContentLength() > 0) {
                        profilePath = "http://WIDzard.pwneo.com/files/member_extra_info/profile_image/"
                                + newFeedItems.get(i).getNew_member_srl() + "/" + newFeedItems.get(i).getMember_srl() + ".gif";

                    } else {
                        profilePath = "";
                    }
                    newFeedItems.get(i).setProfilePic(profilePath);
                    visibleItems.add(newFeedItems.get(i));
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return profilePath;
        }
        @Override
        protected void onPostExecute(String result) {
            Log.e(TAG,"PictureReadTask Done");
            listAdapter.notifyDataSetChanged();
            isLoaded=true;
        }
    }


}
