package com.sunhokim.widzard.sunhokimproject.FeedList;

public class FeedItem {
	private int id, member_srl;
	protected String name, status, image, profilePic, timeStamp, url;
    protected String readCount,commentCount, txtHtml,document_srl, comment_srl,parent_srl,list_order;
    protected String module_srl, is_notice, signature, new_member_srl, user_id;
    protected boolean isSorted, isMine;
	public FeedItem() {
	}

	public FeedItem(int id, String name, String image, String status,
			String profilePic, String timeStamp, String url, String readCount, String commentCount,
            String txtHtml, String document_srl, String parent_srl, String comment_srl, String user_id,
            boolean isSorted, String list_order, String  module_srl, String is_notice, String signature,
            String new_member_srl, int member_srl, boolean isMine) {
		super();
		this.id = id;
		this.name = name;
		this.image = image;
		this.status = status;
		this.profilePic = profilePic;
		this.timeStamp = timeStamp;
		this.url = url;
        this.readCount = readCount;
        this.commentCount = commentCount;
        this.txtHtml = txtHtml;
        this.document_srl = document_srl;
        this.parent_srl = parent_srl;
        this.comment_srl = comment_srl;
        this.isSorted = isSorted;
        this.list_order = list_order;
        this.module_srl = module_srl;
        this.is_notice = is_notice;
        this.signature = signature;
        this.new_member_srl = new_member_srl;
        this.member_srl = member_srl;
        this.user_id = user_id;
        this.isMine = isMine;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

    public String getNew_member_srl() { return new_member_srl; }

    public void setNew_member_srl(String new_member_srl) { this.new_member_srl = new_member_srl; }

    public String getUser_id() { return user_id; }

    public void setUser_id(String user_id) { this.user_id = user_id; }

    public int getMember_srl() { return member_srl; }

    public void setMember_srl(int member_srl) { this.member_srl = member_srl; }

    public String getList_order() { return list_order; }

    public void setList_order(String list_order) { this.list_order = list_order; }

    public boolean getIsSorted() { return isSorted; }

    public void setIsSorted(boolean isSorted) { this.isSorted = isSorted; }

    public boolean getIsMine() { return isMine; }

    public void setIsMine(boolean isMine) { this.isMine = isMine; }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getIs_notice() {
        return is_notice;
    }

    public void setIs_notice(String is_notice) {
        this.is_notice = is_notice;
    }

    public String getComment_srl() {
        return comment_srl;
    }

    public void setComment_srl(String comment_srl) {
        this.comment_srl = comment_srl;
    }

    public String getModule_srl() {
        return module_srl;
    }

    public void setModule_srl(String module_srl) {
        this.module_srl = module_srl;
    }

    public String getParent_srl() {
        return parent_srl;
    }

    public void setParent_srl(String parent_srl) {
        this.parent_srl = parent_srl;
    }

    public String getDocument_srl() {
        return document_srl;
    }

    public void setDocument_srl(String document_srl) {
        this.document_srl = document_srl;
    }

    public String getTxtHtml() {
        return txtHtml;
    }

    public void setTxtHtml(String txtHtml) {
        this.txtHtml = txtHtml;
    }

    public String getReadCount() {
        return readCount;
    }

    public void setReadCount(String readCount) {
        this.readCount = readCount;
    }

    public String getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(String commentCount) {
        this.commentCount = commentCount;
    }

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getProfilePic() {
		return profilePic;
	}

	public void setProfilePic(String profilePic) {
		this.profilePic = profilePic;
	}

	public String getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}
