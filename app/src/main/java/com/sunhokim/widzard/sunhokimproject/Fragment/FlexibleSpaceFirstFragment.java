package com.sunhokim.widzard.sunhokimproject.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sunhokim.widzard.sunhokimproject.R;

/**
 * Created by SunhoKim on 2015-04-27.
 */
public class FlexibleSpaceFirstFragment extends Fragment {

    public FlexibleSpaceFirstFragment(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home, container,
                false);

        return rootView;
    }

}
