package com.sunhokim.widzard.sunhokimproject.Fragment;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.Toast;


import com.sunhokim.widzard.sunhokimproject.MainActivity;
import com.sunhokim.widzard.sunhokimproject.R;

/**
 * Created by SunhoKim on 2015-04-14.
 */
public class SettingFragment extends Fragment {


    private CheckBox mCheckBox;
    SharedPreferences.Editor editor;
    SharedPreferences pref;
    public SettingFragment(){}

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mCheckBox.setChecked(MainActivity.autoLogin);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_setting, container,
                false);

        RelativeLayout rl = (RelativeLayout) rootView.findViewById(R.id.rl1);
        RelativeLayout r2 = (RelativeLayout) rootView.findViewById(R.id.rl2);
        mCheckBox = (CheckBox) rootView.findViewById(R.id.checkBox);
        pref = MainActivity.mActivity.getSharedPreferences("pref", Activity.MODE_PRIVATE);
        if(pref.getBoolean("auto",false))
            mCheckBox.setChecked(true);
        else
            mCheckBox.setChecked(false);
        mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                editor = pref.edit();
                editor.putBoolean("auto", isChecked);
                editor.commit();
                MainActivity.autoLogin = isChecked;
                //Log.d("TAG","CheckBox clicked! => "+isChecked);
            }
        });

        rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(MainActivity.mContext, "Layer Clicked",Toast.LENGTH_SHORT).show();
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.container, new LoginFragment())
                        .addToBackStack("detail").commit();

            }
        });
        r2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(pref.getBoolean("auto",false))
                {
                    editor = pref.edit();
                    editor.putBoolean("auto", false);
                    editor.commit();
                    MainActivity.autoLogin = false;
                    mCheckBox.setChecked(false);
                    //Log.d("TAG","CheckBox clicked! => "+false);
                }
                else
                {
                    editor = pref.edit();
                    editor.putBoolean("auto", true);
                    editor.commit();
                    MainActivity.autoLogin = true;
                    mCheckBox.setChecked(true);
                    //Log.d("TAG","CheckBox clicked! => "+true);
                }
            }
        });
        return rootView;
    }

}
