package com.sunhokim.widzard.sunhokimproject.Fragment;


import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.sunhokim.widzard.sunhokimproject.FeedList.FeedItem;
import com.sunhokim.widzard.sunhokimproject.FeedList.FeedListAdapter;
import com.sunhokim.widzard.sunhokimproject.MainActivity;
import com.sunhokim.widzard.sunhokimproject.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.transitions.everywhere.*;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by SunhoKim on 2015-03-25.
 */
public class HomeFragment extends Fragment {
    private static final String TAG = MainActivity.class.getSimpleName();
    private ListView listView;
    private FeedListAdapter listAdapter;
    private List<FeedItem> feedItems;
    private String jsonResult;
    private String URL_FEED = "https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&maxResults=50&playlistId=PL0WJGLjHbQEIF2LjommyP5E9EDRvKNUcR&key=AIzaSyBHZ7BSLfM7ktpPRUN1EZRjb76MOU_kWDE";
    private String mButtonId;
    private View buttonFloat;
    private Scene mScene1;

    public HomeFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home, container,
                false);
        listView = (ListView) rootView.findViewById(R.id.list);
        feedItems = new ArrayList<FeedItem>();
        listAdapter = new FeedListAdapter(MainActivity.mActivity, feedItems);
        listView.setAdapter(listAdapter);
        accessWebService();
        buttonFloat = rootView.findViewById(R.id.buttonFloat);
        // You can also inflate a generate a Scene from a layout resource file.
        mScene1 = Scene.getSceneForLayout(container, R.layout.fragment_login, MainActivity.mActivity);

        buttonFloat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TransitionSet set = new TransitionSet();
                Slide slide = new Slide(Gravity.START);
                slide.addTarget(R.id.buttonFloat);
                set.addTransition(slide);
                set.addTransition(new ChangeBounds());
                set.addTransition(new ChangeImageTransform());

                set.setDuration(350);
                TransitionManager.go(mScene1, set);
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, new LoginFragment())
                        .commit();

            }


        });
        return rootView;
    }
    public void accessWebService() {
        JsonReadTask task = new JsonReadTask();
        // passes values for the urls string array
        task.execute();
    }
    // getStringFromUrl : 주어진 URL 페이지를 문자열로 얻는다.
    public static String getStringFromUrl(String url) throws UnsupportedEncodingException {
        // 입력스트림을 "UTF-8" 를 사용해서 읽은 후, 라인 단위로 데이터를 읽을 수 있는 BufferedReader 를 생성한다.
        BufferedReader br = new BufferedReader(new InputStreamReader(getInputStreamFromUrl(url), "UTF-8"));
        StringBuffer sb = new StringBuffer();

        try {
            String line = null;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return sb.toString();
    } // getStringFromUrl

    // getInputStreamFromUrl : 주어진 URL 에 대한 입력 스트림(InputStream)을 얻는다.
    public static InputStream getInputStreamFromUrl(String url) {
        InputStream contentStream = null;
        try {
            // HttpClient 를 사용해서 주어진 URL에 대한 입력 스트림을 얻는다.
            HttpClient httpclient = new DefaultHttpClient();
            HttpResponse response = httpclient.execute(new HttpGet(url));
            contentStream = response.getEntity().getContent();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return contentStream;
    } // getInputStreamFromUrl
    final Handler handler = new Handler()
    {
        public void handleMessage(Message msg)
        {
            listAdapter.notifyDataSetChanged();
        }
    };
    private void CheckDataJson() {

        JSONObject object, object2 = null;
        JSONArray feedArray=null;
        JSONArray feedArray2 = null;
        String line = null;
        try {
            line = getStringFromUrl(URL_FEED);
            Log.e(TAG,line);
            object = new JSONObject(line);
            feedArray = object.optJSONArray("items");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            for (int i = 0; i < feedArray.length(); i++) {
                try {
                    FeedItem item = new FeedItem();
                    JSONObject obj = feedArray.getJSONObject(i);
                    JSONObject snippet = obj.getJSONObject("snippet");
                    JSONObject resourceId = snippet.getJSONObject("resourceId");
                    JSONObject thumbnails = snippet.getJSONObject("thumbnails");
                    JSONObject thumbnails_url = thumbnails.getJSONObject("standard");
                    Log.e(TAG,"id: "+obj.getString("id")+" title: "+snippet.getString("title")+" VideoID: "+resourceId.getString("videoId"));
                    item.setId(snippet.getInt("position"));
                    item.setStatus(snippet.getString("title"));
                    item.setImage(thumbnails_url.getString("url"));
                    item.setName("☞성공의 조짐");
                    item.setProfilePic("https://yt3.ggpht.com/-zt7nTGWKb_E/AAAAAAAAAAI/AAAAAAAAAAA/NrOKbXxHP1c/s100-c-k-no/photo.jpg");
                    try {
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                        String convertTime = snippet.getString("publishedAt");
                        convertTime=convertTime.replace("T"," ");
                        convertTime=convertTime.replace("Z"," ");
                        Date parsedDate = dateFormat.parse(convertTime);
                        Timestamp timeStamp = new java.sql.Timestamp(parsedDate.getTime());
                        item.setTimeStamp(parsedDate.getTime()+"");
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    feedItems.add(item);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

        }catch (NullPointerException e){
            e.printStackTrace();

        }

        new Thread()
        {
            public void run()
            {
                Message msg = handler.obtainMessage();
                handler.sendMessage(msg);
            }
        }.start();

    }

    private class JsonReadTask extends AsyncTask<Void, Void, Void> {

        protected void onPreExecute() {
        }

        @Override
        protected Void doInBackground(Void... params) {
            CheckDataJson();

            return (null);
        }

        @Override
        protected void onPostExecute(Void unused) {

        }
    }
}
