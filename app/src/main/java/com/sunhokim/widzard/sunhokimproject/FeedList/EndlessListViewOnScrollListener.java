package com.sunhokim.widzard.sunhokimproject.FeedList;

import android.util.Log;
import android.widget.AbsListView;

/**
 * Created by SunhoKim on 2015-05-06.
 */
public abstract class EndlessListViewOnScrollListener implements AbsListView.OnScrollListener {
    public static final boolean DEBUG = true;
    private int visibleThreshold = 5;
    private int currentPage = 0;
    private int previousTotalItemCount = 0;
    private boolean loading = true;
    private int startingPageIndex = 0;

    public EndlessListViewOnScrollListener() {
    }

    public EndlessListViewOnScrollListener(int visibleThreshold) {
        this.visibleThreshold = visibleThreshold;
    }

    public EndlessListViewOnScrollListener(int visibleThreshold, int startPage) {
        this.visibleThreshold = visibleThreshold;
        this.startingPageIndex = startPage;
        this.currentPage = startPage;
    }
    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

        if (totalItemCount < previousTotalItemCount) {
            this.currentPage = this.startingPageIndex;
            this.previousTotalItemCount = totalItemCount;
            if (totalItemCount == 0) {
                this.loading = true;
            }
        }

        if (loading && (totalItemCount > previousTotalItemCount)) {
            loading = false;
            previousTotalItemCount = totalItemCount;
            currentPage++;
        }

        if (!loading && (firstVisibleItem + visibleItemCount+5 >= totalItemCount) && (totalItemCount - 1 > 0)) {
            if (visibleItemCount != 1) {
                onLoadMore(currentPage, totalItemCount);
                loading = true;
            }
        }

    }

    public abstract void onLoadMore(int page, int totalItemsCount);


    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {
    }
}