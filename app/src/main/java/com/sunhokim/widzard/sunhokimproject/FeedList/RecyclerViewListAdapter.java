package com.sunhokim.widzard.sunhokimproject.FeedList;

/**
 * Created by SunhoKim on 2015-04-02.
 */

import android.app.Activity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.format.DateUtils;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.marshalchen.ultimaterecyclerview.UltimateViewAdapter;
import com.sunhokim.widzard.sunhokimproject.MainActivity;
import com.sunhokim.widzard.sunhokimproject.R;
import com.sunhokim.widzard.sunhokimproject.app.AppController;
import com.sunhokim.widzard.sunhokimproject.app.CircularNetworkImageView;

import java.util.List;

public class RecyclerViewListAdapter extends UltimateViewAdapter {
   //private List<ContactInfo> contactList;
   private static final String TAG = MainActivity.class.getSimpleName();
    private LayoutInflater inflater;
    private List<FeedItem> feedItems;
    private Activity activity;
    ImageLoader imageLoader = AppController.getInstance().getImageLoader();
    
    public RecyclerViewListAdapter(Activity activity, List<FeedItem> feedItems) {
        //this.contactList = contactList;
        this.activity = activity;
        this.feedItems = feedItems;
    }
    @Override
    public int getItemCount()  {
        return feedItems.size();
    }

    @Override
    public int getAdapterItemCount() {
        return 0;
    }

    public FeedItem getItem(int location) {
        return feedItems.get(location);
    }



    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        FeedItem item = feedItems.get(position);

        ((ViewHolder) holder).name.setText(item.getName());
        ((ViewHolder) holder).statusMsg.setText(item.getStatus());
        ((ViewHolder) holder).readCount.setText(item.getReadCount()+" Views");
        ((ViewHolder) holder).commentCount.setText(item.getCommentCount()+" Comments");
        CharSequence timeAgo = DateUtils.getRelativeTimeSpanString(
                Long.parseLong(item.getTimeStamp()),
                System.currentTimeMillis(), DateUtils.HOUR_IN_MILLIS);
        ((ViewHolder) holder).timestamp.setText(timeAgo);
        // Checking for null feed url
        if (item.getUrl() != null) {
            ((ViewHolder) holder).url.setText(Html.fromHtml("<a href=\"" + item.getUrl() + "\">"
                    + item.getUrl() + "</a> "));

            // Making url clickable
            ((ViewHolder) holder).url.setMovementMethod(LinkMovementMethod.getInstance());
            ((ViewHolder) holder).url.setVisibility(View.VISIBLE);
        } else {
            // url is null, remove from the view
            ((ViewHolder) holder).url.setVisibility(View.GONE);
        }
        ((ViewHolder) holder).profilePic.setImageUrl(item.getProfilePic(), imageLoader);
        // Feed image
        if (item.getImage() != null) {
            ((ViewHolder) holder).feedImageView.setImageUrl(item.getImage(), imageLoader);
            ((ViewHolder) holder).feedImageView.setVisibility(View.VISIBLE);
            ((ViewHolder) holder).feedImageView
                    .setResponseObserver(new FeedImageView.ResponseObserver() {
                        @Override
                        public void onError() {
                        }

                        @Override
                        public void onSuccess() {
                        }
                    });
        } else {
            ((ViewHolder) holder).feedImageView.setVisibility(View.GONE);
        }
        if(item.getIs_notice().equals("Y")){
            ((ViewHolder) holder).cardView.setCardBackgroundColor(MainActivity.mActivity.getResources().getColor(R.color.sunFlower));
            ((ViewHolder) holder).readCount.setVisibility(View.GONE);
            ((ViewHolder) holder).commentCount.setVisibility(View.GONE);
        }else{
            ((ViewHolder) holder).cardView.setCardBackgroundColor(MainActivity.mActivity.getResources().getColor(R.color.white));
            ((ViewHolder) holder).readCount.setVisibility(View.VISIBLE);
            ((ViewHolder) holder).commentCount.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.feed_item_cardview, viewGroup, false);

        return new ViewHolder(itemView);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        protected TextView name;
        protected TextView timestamp;
        protected TextView statusMsg;
        protected TextView url;
        protected CircularNetworkImageView profilePic;
        protected FeedImageView feedImageView;
        protected TextView readCount;
        protected TextView commentCount;
        protected CardView cardView;
        public ViewHolder(View convertView) {
            super(convertView);
            cardView = (CardView) convertView.findViewById(R.id.card_view);
            name = (TextView) convertView.findViewById(R.id.name);
            timestamp = (TextView) convertView
                    .findViewById(R.id.timestamp);
            readCount = (TextView) convertView.findViewById(R.id.readCount);
            commentCount =  (TextView) convertView.findViewById(R.id.commentCount);
            statusMsg = (TextView) convertView
                    .findViewById(R.id.txtStatusMsg);
            url = (TextView) convertView.findViewById(R.id.txtUrl);
            profilePic = (CircularNetworkImageView) convertView
                    .findViewById(R.id.profilePic);
            feedImageView = (FeedImageView) convertView
                    .findViewById(R.id.feedImage1);
        }

        @Override
        public void onClick(View v) {

        }
    }

}