package com.sunhokim.widzard.sunhokimproject.FeedList;

/**
 * Created by SunhoKim on 2015-04-02.
 */

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.format.DateUtils;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.sunhokim.widzard.sunhokimproject.MainActivity;
import com.sunhokim.widzard.sunhokimproject.R;
import com.sunhokim.widzard.sunhokimproject.app.AppController;
import com.sunhokim.widzard.sunhokimproject.app.CircularNetworkImageView;

import java.util.List;
public class RecyclerViewCommentAdapter extends BaseAdapter {
    //private List<ContactInfo> contactList;
    private static final String TAG = MainActivity.class.getSimpleName();
    private LayoutInflater inflater;
    private List<FeedItem> feedItems;
    private Activity activity;
    ImageLoader imageLoader = AppController.getInstance().getImageLoader();

    public RecyclerViewCommentAdapter(Activity activity, List<FeedItem> feedItems) {
        //this.contactList = contactList;
        this.activity = activity;
        this.feedItems = feedItems;
    }
	@Override
	public int getCount() {
		return feedItems.size();
	}


	public FeedItem getItem(int location) {
        return feedItems.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


		if (inflater == null)
			inflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		if (convertView == null)
			convertView = inflater.inflate(R.layout.feed_item_comment, null);

		if (imageLoader == null)
			imageLoader = AppController.getInstance().getImageLoader();

        LinearLayout commentLayer = (LinearLayout) convertView.findViewById(R.id.commentLayer);

        commentLayer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.mContext, "Clicked!", Toast.LENGTH_SHORT).show();
            }
        });
		TextView name = (TextView) convertView.findViewById(R.id.name);
		TextView timestamp = (TextView) convertView
				.findViewById(R.id.timestamp);
		TextView statusMsg = (TextView) convertView
				.findViewById(R.id.txtStatusMsg);
		CircularNetworkImageView profilePic = (CircularNetworkImageView) convertView
				.findViewById(R.id.profilePic);
				
		FeedItem item = feedItems.get(position);
        if(item.getIsMine()) commentLayer.setBackgroundColor(MainActivity.mActivity.getResources().getColor(R.color.sunFlower));
        name.setText(item.getName());
        statusMsg.setText(item.getStatus());
        CharSequence timeAgo = DateUtils.getRelativeTimeSpanString(
                Long.parseLong(item.getTimeStamp()),
                System.currentTimeMillis(), DateUtils.HOUR_IN_MILLIS);
        timestamp.setText(timeAgo);
        profilePic.setImageUrl(item.getProfilePic(), imageLoader);

        return convertView;
    }



}