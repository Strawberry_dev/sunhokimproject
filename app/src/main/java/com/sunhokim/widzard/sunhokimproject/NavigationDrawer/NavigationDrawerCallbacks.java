package com.sunhokim.widzard.sunhokimproject.NavigationDrawer;

public interface NavigationDrawerCallbacks {
    void onNavigationDrawerItemSelected(int position);
}
