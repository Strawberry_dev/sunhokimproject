package com.sunhokim.widzard.sunhokimproject.Fragment;

import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.dd.processbutton.iml.ActionProcessButton;
import com.rey.material.widget.EditText;
import com.sunhokim.widzard.sunhokimproject.MainActivity;
import com.sunhokim.widzard.sunhokimproject.ProgressGenerator;
import com.sunhokim.widzard.sunhokimproject.R;
import com.sunhokim.widzard.sunhokimproject.app.DataNetworkUtil;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import me.drakeet.materialdialog.MaterialDialog;

/**
 * Created by SunhoKim on 2015-04-02.
 */
public class LoginFragment extends Fragment implements ProgressGenerator.OnCompleteListener{


    private static final String TAG = MainActivity.class.getSimpleName();
    public static final String EXTRAS_ENDLESS_MODE = "EXTRAS_ENDLESS_MODE";
    private static MaterialDialog mMaterialDialog;

    private static String URL_FEED = "http://WIDzard.pwneo.com/WIDzardLoginCheck.php";
    private String jsonResult;
    EditText editId, editPw;
    ActionProcessButton btnSignIn;
    ProgressGenerator progressGenerator;
    CookieManager cookieManager;

    public static boolean isAutoLogin = false;
    public static String userId=null;
    public static String userPw = null;

    String[] userData;
    static boolean  isSelfLogin = false;

    public LoginFragment() {
    }
    public static LoginFragment newInstance(String id, String pw, boolean auto) {

        LoginFragment fragment = new LoginFragment();
        userId=id;
        userPw=pw;
        isAutoLogin = auto;
        //fragment.accessWebService();
        isSelfLogin = false;

        CookieSyncManager.createInstance(MainActivity.mContext);
        if(!isAutoLogin) CookieManager.getInstance().removeAllCookie();
        CookieSyncManager.getInstance().startSync();
        MainActivity.dataNetworkStatus = DataNetworkUtil.getConnectivityStatus(MainActivity.mContext);
        try{
            if(MainActivity.dataNetworkStatus!=0)
                fragment.loginTask(URL_FEED);
            else {
                mMaterialDialog = new MaterialDialog(MainActivity.mContext)
                        .setTitle("인터넷 연결 상태 확인")
                        .setMessage("인터넷 연결 상태를 확인해주세요.\n")
                        .setPositiveButton("확인", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mMaterialDialog.dismiss();
                            }
                        });
                mMaterialDialog.show();
            }
        }catch (Exception e) {

            e.printStackTrace();
        }
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_login, container,
                false);

        CookieSyncManager.createInstance(MainActivity.mContext);
        editId = (EditText) rootView.findViewById(R.id.loginId);
        editPw = (EditText) rootView.findViewById(R.id.loginPassword);
        if(MainActivity.autoLogin) {
            if (!MainActivity.userId.isEmpty())
                editId.setText(MainActivity.userId);
            if (!MainActivity.userPw.isEmpty())
                editPw.setText(MainActivity.userPw);
        }
        editId.addTextChangedListener(new TextWatcher() {
            //입력하기 전에
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            //입력되는 텍스트에 변화가 있을때
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!MainActivity.isLoginUser) btnSignIn.setProgress(0);
            }
            //입력이 끝났을때
            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        editId.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!MainActivity.isLoginUser) btnSignIn.setProgress(0);
            }
        });

        editPw.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!MainActivity.isLoginUser) btnSignIn.setProgress(0);
            }
        });
        editPw.addTextChangedListener(new TextWatcher() {
            //입력하기 전에
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            //입력되는 텍스트에 변화가 있을때
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!MainActivity.isLoginUser) btnSignIn.setProgress(0);
            }

            //입력이 끝났을때
            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        progressGenerator = new ProgressGenerator(this);
        btnSignIn = (ActionProcessButton) rootView.findViewById(R.id.btnSignIn);
        btnSignIn.setMode(ActionProcessButton.Mode.ENDLESS);
        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CookieSyncManager.createInstance(MainActivity.mContext);
                CookieManager.getInstance().removeAllCookie();
                CookieSyncManager.getInstance().startSync();
                userId = editId.getText().toString();
                userPw = editPw.getText().toString();
                isSelfLogin = true;
                MainActivity.dataNetworkStatus = DataNetworkUtil.getConnectivityStatus(MainActivity.mContext);
                try {
                    if (MainActivity.dataNetworkStatus != 0 &&
                    !editId.getText().toString().isEmpty() && !editPw.getText().toString().isEmpty() )
                    loginTask(URL_FEED);
                    else{
                        mMaterialDialog = new MaterialDialog(MainActivity.mContext)
                                .setTitle("인터넷 연결 상태 확인")
                                .setMessage("인터넷 연결 상태를 확인해주세요.\n")
                                .setPositiveButton("확인", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        mMaterialDialog.dismiss();
                                    }
                                });
                        mMaterialDialog.show();
                    }
                } catch (Exception e) {

                    e.printStackTrace();
                }
                }

        });

        return rootView;
    }
    private StringBuilder inputStreamToString(InputStream is) {
        String rLine = "";
        StringBuilder answer = new StringBuilder();
        BufferedReader rd = new BufferedReader(new InputStreamReader(is));

        try {
            while ((rLine = rd.readLine()) != null) {
                answer.append(rLine);
            }
        }

        catch (IOException e) {
            e.printStackTrace();

        }
        return answer;
    }
    public void loginTask(String URL_FEED) {
        cookieManager = CookieManager.getInstance();
        HttpParams httpParams = new BasicHttpParams();
        httpParams.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
        HttpClient httpclient = new DefaultHttpClient(httpParams);
        HttpPost httppost = new HttpPost(URL_FEED);
        try {
            //전달할 인자들
            Vector<NameValuePair> nameValue = new Vector<NameValuePair>();
            nameValue.add(new BasicNameValuePair("user_id", userId));
            nameValue.add(new BasicNameValuePair("password", userPw));
            //웹 접속 - utf-8 방식으로
            HttpEntity enty = new UrlEncodedFormEntity(nameValue, HTTP.UTF_8);
            httppost.setEntity(enty);
            HttpResponse response = httpclient.execute(httppost);
            jsonResult = inputStreamToString(
                    response.getEntity().getContent()).toString();

            MainActivity.isLoginUser = false;

            if(!(jsonResult.equalsIgnoreCase("error") || jsonResult.isEmpty()))
            {
                MainActivity.isLoginUser = true;
                jsonResult = jsonResult.replace("WIDzardLoginCheck.php","");
                Log.d(TAG,jsonResult);
                userData = jsonResult.split("&");
                SharedPreferences pref = MainActivity.mActivity.getSharedPreferences("pref",MainActivity.mActivity.MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();
                if(!isAutoLogin) {
                    editor.putString("id", editId.getText().toString());
                    editor.putString("password", editPw.getText().toString());
                    editor.commit();
                }
                List<Cookie> cookies = ((DefaultHttpClient)httpclient).getCookieStore().getCookies();
                if (!cookies.isEmpty()) {
                    for (int i = 0; i < cookies.size(); i++) {
                        String cookieString = cookies.get(i).getName() + "=" + cookies.get(i).getValue();
                        //Log.d(TAG,"cookie: "+cookieString);
                        cookieManager.setCookie(URL_FEED, cookieString);
                    }
                }
                CookieSyncManager.getInstance().sync();
            }
        }

        catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(MainActivity.isLoginUser)  {
            if(isSelfLogin) progressGenerator.start(btnSignIn);
            else Toast.makeText(MainActivity.mContext, userData[0]+"님 어서오세요.", Toast.LENGTH_SHORT).show();
            MainActivity.userNickName = userData[0];
            if(userData.length==2)
                MainActivity.userProfilePic = userData[1];

            else
                MainActivity.userProfilePic = null;
            MainActivity.userId = userId;
            MainActivity.userPw = userPw;
        }
        else {
            if(isSelfLogin) btnSignIn.setProgress(-1);
            Toast.makeText(MainActivity.mContext, "로그인에 실패하셨습니다.", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onComplete() {
        if(userData.length==2)
            MainActivity.mNavigationDrawerFragment.setUserData(MainActivity.userNickName, "어서오세요.", userData[1]);
        else
            MainActivity.mNavigationDrawerFragment.setUserData(MainActivity.userNickName, "어서오세요.", null);
        //MainActivity.mNavigationDrawerFragment.setUserData(MainActivity.userNickName, "어서오세요.", userData[1]);
    }



    @Override
    public void onPause()
    {
        super.onPause();
        CookieSyncManager.getInstance().stopSync();
    }

    @Override
    public void onResume()
    {
        super.onResume();

        CookieSyncManager.getInstance().startSync();
    }

}


